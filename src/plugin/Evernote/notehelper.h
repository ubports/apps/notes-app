/*
 * Copyright: 2020 UBports
 *
 * This file is part of reminders
 *
 * reminders is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * reminders is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Authors: Brian Douglass
 */

#ifndef NOTEHELPER_H
#define NOTEHELPER_H

#include <QObject>
#include <QString>

class NoteHelper: public QObject
{
    Q_OBJECT

    Q_PROPERTY(QString notePath READ notePath CONSTANT)
    Q_PROPERTY(int port READ port CONSTANT)

public:
    static NoteHelper *instance();

    int port() const;
    QString notePath() const;
    Q_INVOKABLE void writeNote(QString html, QString backgroundColor, QString textColor) const;

private:
    NoteHelper(QObject *parent = 0);
    static NoteHelper *s_instance;
    int m_port;
};

#endif
