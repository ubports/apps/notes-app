/*
 * Copyright: 2019 UBports
 *
 * This file is part of reminders
 *
 * reminders is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * reminders is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import QtQuick 2.4
import Lomiri.Components 1.3
import Lomiri.Components.ListItems 1.3 as ListItem

import "../components"
import "settings"

Page {
    id: root
    objectName: "settingsPage"

    header: PageHeader {
        id: header
        title: i18n.tr("Settings")
        trailingActionBar.actions: [
            Action {
                text: i18n.tr("Help")
                iconName: "help"

                onTriggered: {
                    apl.addPageToNextColumn(root, Qt.resolvedUrl("HelpPage.qml"))
                }
            }
        ]
    }

    Column {
        anchors.top: header.bottom
        anchors.left: parent.left
        anchors.right: parent.right

        NotesAppListItem {
            titleText: i18n.tr("Account")
            subtitleText: preferences.accountName
            iconName: "account"
            action: Action {
                onTriggered: {
                    apl.addPageToNextColumn(root, Qt.resolvedUrl("AccountSelectorPage.qml"), { accounts: accounts })
                }
            }
        }

        ListItem.Standard {
            height: units.gu(8)
            text: i18n.tr("Notebook colors")
            control: Switch {
                id: colorSettingsSwitch
                checked: settings.notebookColorsEnabled

                onCheckedChanged: {
                    settings.notebookColorsEnabled = colorSettingsSwitch.checked
                }
            }
        }

        ThemeSettings {
        }
    }
}
