/*
 * Copyright: 2013 - 2014 Canonical, Ltd
 *
 * This file is part of reminders
 *
 * reminders is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * reminders is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import QtQuick 2.4
import QtQuick.Layouts 1.1
import QtQuick.Controls.Suru 2.2
import Lomiri.Components 1.3
import Lomiri.Components.ListItems 1.3
import Lomiri.Components.Popups 1.3
import Evernote 0.1

import "../components"

Item {
    id: root
    objectName: "notesPage"

    property var page
    property alias filterNotebookGuid: notes.filterNotebookGuid
    property alias filterTagGuid: notes.filterTagGuid
    property alias onlySearchResults: notes.onlySearchResults

    Notes {
        id: notes
    }

    LomiriListView {
        id: notesListView
        anchors.fill: parent
        pullToRefresh.enabled: preferences.accountName != "@local"
        pullToRefresh.refreshing: notes.loading
        pullToRefresh.onRefresh: notes.refresh()
        currentIndex: -1 // Workaround: LomiriListView seems to auto-highlight the 1st item
        clip: true
        model: notes
        delegate: NoteDelegate {
            note: NotesStore.note(model.guid)

            onOpenNote: {
                var note = NotesStore.note(model.guid)

                notesListView.currentIndex = index
                displayNote(note, root.page)
            }
            onDeleteNote: {
                var confirmation = PopupUtils.open(Qt.resolvedUrl("../components/DeleteConfirmationDialog.qml"), root,
                                                   { targetType: i18n.tr("note"), targetTitle: model.title })

                confirmation.confirmed.connect(function() {
                    if(notesListView.currentIndex == index) {
                        notesListView.currentIndex = -1
                        apl.removePages(root.page)
                    }

                    NotesStore.deleteNote(model.guid)
                })
            }
            onEditNote: {
                notesListView.currentIndex = index
                apl.addPageToNextColumn(root.page, Qt.resolvedUrl("EditNotePage.qml"), { note: NotesStore.note(model.guid) })
            }
            onOpenFilters: {
                notesListView.currentIndex = index
                apl.addPageToNextColumn(root.page, Qt.resolvedUrl("EditNoteFiltersPage.qml"), { note: NotesStore.note(model.guid) })
            }

            Component.onCompleted: {
                NotesStore.note(model.guid).load(false)
            }
        }
    }

    Label {
        anchors.centerIn: parent
        visible: !notes.loading && notesListView.count == 0
        width: parent.width - units.gu(4)
        wrapMode: Text.WordWrap
        horizontalAlignment: Text.AlignHCenter
        text: root.onlySearchResults ? "" : i18n.tr("No notes available")
    }

    Connections {
        target: NotesStore

        onNoteCreated: {
            var note = NotesStore.note(guid)

            notesListView.currentIndex = 0
            apl.addPageToNextColumn(root.page, Qt.resolvedUrl("EditNotePage.qml"), { note: note })
        }
    }
}
