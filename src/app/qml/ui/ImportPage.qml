/*
 * Copyright: 2021 UBports
 *
 * This file is part of reminders
 *
 * reminders is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * reminders is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import QtQuick 2.4
import Lomiri.Components 1.3
import Lomiri.Content 1.3

Page {
    id: root

    property var note
    property var noteTextArea
    property int insertPosition
    property var activeTransfer: null

    header: PageHeader {
        id: header
        title: i18n.tr("Import image from")
    }

    ContentPeerPicker {
        anchors.top: header.bottom
        anchors.bottom: parent.bottom
        anchors.left: parent.left
        anchors.right: parent.right
        contentType: ContentType.Pictures
        handler: ContentHandler.Source
        showTitle: false

        onPeerSelected: {
            peer.selectionType = ContentTransfer.Single
            root.activeTransfer = peer.request()
        }
    }

    ContentTransferHint {
        activeTransfer: root.activeTransfer
    }

    Connections {
        target: root.activeTransfer

        onStateChanged: {
            if (root.activeTransfer.state === ContentTransfer.Charged) {
                var file = root.activeTransfer.items[0].url.toString()
                print("attaching file", file, "on note", note)
                root.note.attachFile(root.insertPosition, file)
                root.noteTextArea.insert(root.insertPosition + 1, "<br>&nbsp;")
                apl.removePages(root)
            }
        }
    }
}
