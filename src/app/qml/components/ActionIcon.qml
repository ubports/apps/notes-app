/*
 * Copyright: 2021 UBports
 *
 * This file is part of reminders
 *
 * reminders is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * reminders is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import QtQuick 2.4
import Lomiri.Components 1.3

Item {
    id: root
    width: units.gu(4)
    height: parent.height

    property alias iconName: _icon.name
    property int index: -1
    property int selectedIndex: -1

    signal selected()

    AbstractButton {
        id: _button
        anchors.fill: parent

        property bool focused: root.index == root.selectedIndex

        Column {
            anchors.fill: parent
            anchors.topMargin: (parent.height - _icon.height - _rec.height) / 2
            spacing: anchors.topMargin

            Icon {
                id: _icon
                anchors.horizontalCenter: parent.horizontalCenter
                width: units.gu(2)
                height: width
                color: _button.focused ? theme.palette.selected.focus : theme.palette.normal.base
            }

            Rectangle {
                id: _rec
                width: parent.width
                height: units.gu(0.4)
                color: theme.palette.selected.focus
                visible: _button.focused
            }
        }

        onClicked: root.selected()
    }
}
