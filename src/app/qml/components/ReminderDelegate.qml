/*
 * Copyright: 2013 Canonical, Ltd
 *
 * This file is part of reminders-app
 *
 * reminders-app is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * reminders-app is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import QtQuick 2.4
import Lomiri.Components 1.3
import Lomiri.Components.ListItems 1.3

ListItem {
    id: root
    divider.visible: false

    property var note
    property string notebookColor: {
        if (settings.notebookColorsEnabled)
            return preferences.colorForNotebook(note.notebookGuid)
        else
            return theme.palette.normal.foregroundText
    }

    signal openReminder()
    signal deleteReminder()
    signal editReminder()

    leadingActions: ListItemActions {
        actions: [
            Action {
                text: i18n.tr("Delete")
                iconName: "delete"

                onTriggered: root.deleteReminder()
            }
        ]
    }
    action: Action {
        onTriggered: root.openReminder()
    }
    trailingActions: ListItemActions {
        actions: [
            Action {
                text: i18n.tr("Edit")
                iconName: "edit"

                onTriggered: root.editReminder()
            }
        ]
    }

    ListItemLayout {
        width: parent.width
        height: parent.height - reminderDivider.height
        title.text: root.note.title
        title.color: root.notebookColor
        subtitle.text: Qt.formatDateTime(root.note.reminderTime, Qt.LocalDate)
    }

    Rectangle {
        id: reminderDivider
        anchors.bottom: parent.bottom
        width: parent.width
        height: units.gu(0.1)
        color: theme.palette.normal.base
    }
}
