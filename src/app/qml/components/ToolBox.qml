/*
 * Copyright: 2021 UBports
 *
 * This file is part of reminders
 *
 * reminders is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * reminders is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import QtQuick 2.4
import Lomiri.Components 1.3
import Lomiri.Components.ListItems 1.3
import Lomiri.Components.Popups 1.3
import Evernote 0.1

Item {
    id: root
    height: toolbox.height

    property var page

    Column {
        id: toolbox
        spacing: units.gu(1)

        property bool charFormatExpanded: false
        property bool blockFormatExpanded: false

        Row {
            anchors.left: parent.left
            anchors.right: parent.right
            anchors.leftMargin: units.gu(1)
            anchors.rightMargin: units.gu(1)
            height: units.gu(4)
            visible: toolbox.charFormatExpanded

            RtfButton {
                id: fontButton
                text: formattingHelper.fontFamily || i18n.tr("Font")
                height: parent.height
                horizontalAlignment: Text.AlignLeft
                onClicked: {
                    Qt.inputMethod.hide();
                    PopupUtils.open(fontPopoverComponent, fontButton, {selectionStart: noteTextArea.selectionStart, selectionEnd: noteTextArea.selectionEnd})
                }
            }

            RtfButton {
                id: fontSizeButton
                text: formattingHelper.fontSize || i18n.tr("Size")
                height: parent.height
                width: height
                onClicked: {
                    Qt.inputMethod.hide();
                    PopupUtils.open(fontSizePopoverComponent, fontSizeButton, {selectionStart: noteTextArea.selectionStart, selectionEnd: noteTextArea.selectionEnd})
                }
            }

            RtfButton {
                id: colorButton
                height: parent.height
                width: height
                color: formattingHelper.color
                onClicked: {
                    Qt.inputMethod.hide();
                    PopupUtils.open(colorPopoverComponent, colorButton, {selectionStart: noteTextArea.selectionStart, selectionEnd: noteTextArea.selectionEnd})
                }
            }

            RtfButton {
                height: parent.height
                width: height
                // TRANSLATORS: Toolbar button for "Bold"
                text: i18n.tr("B")
                font.bold: true
                font.family: "Serif"
                active: formattingHelper.bold
                onClicked: {
                    formattingHelper.bold = !formattingHelper.bold
                }
            }

            RtfButton {
                height: parent.height
                width: height
                // TRANSLATORS: Toolbar button for "Italic"
                text: i18n.tr("I")
                font.bold: true
                font.italic: true
                font.family: "Serif"
                active: formattingHelper.italic
                onClicked: {
                    formattingHelper.italic = !formattingHelper.italic;
                }
            }

            RtfButton {
                height: parent.height
                width: height
                // TRANSLATORS: Toolbar button for "Underline"
                text: i18n.tr("U")
                font.bold: true
                font.underline: true
                font.family: "Serif"
                active: formattingHelper.underline
                onClicked: {
                    formattingHelper.underline = !formattingHelper.underline;
                }
            }

            RtfButton {
                height: parent.height
                width: height
                // TRANSLATORS: Toolbar button for "Strikeout"
                text: i18n.tr("T")
                font.bold: true
                font.strikeout: true
                font.family: "Serif"
                active: formattingHelper.strikeout
                onClicked: {
                    formattingHelper.strikeout = !formattingHelper.strikeout;
                }
            }
        }

        Row {
            anchors.left: parent.left
            anchors.right: parent.right
            anchors.leftMargin: units.gu(1)
            anchors.rightMargin: units.gu(1)
            height: units.gu(4)
            visible: toolbox.blockFormatExpanded

            RtfButton {
                iconSource: "../images/bullet-list.svg"
                height: parent.height
                width: height
                active: formattingHelper.bulletList
                onClicked: {
                    formattingHelper.bulletList = !formattingHelper.bulletList;
                }
            }

            RtfButton {
                iconSource: "../images/numbered-list.svg"
                height: parent.height
                width: height
                active: formattingHelper.numberedList
                onClicked: {
                    formattingHelper.numberedList = !formattingHelper.numberedList;
                }
            }

            RtfSeparator {}

            RtfButton {
                height: parent.height
                width: height
                iconSource: "../images/indent-block.svg"
                onClicked: {
                    formattingHelper.indentBlock();
                }
            }

            RtfButton {
                height: parent.height
                width: height
                iconSource: "../images/unindent-block.svg"
                onClicked: {
                    formattingHelper.unindentBlock();
                }
            }

            RtfSeparator {}

            RtfButton {
                height: parent.height
                width: height
                iconSource: "../images/left-align.svg"
                active: formattingHelper.alignment & Qt.AlignLeft
                onClicked: {
                    formattingHelper.alignment = Qt.AlignLeft
                }
            }

            RtfButton {
                height: parent.height
                width: height
                iconSource: "../images/center-align.svg"
                active: formattingHelper.alignment & Qt.AlignHCenter
                onClicked: {
                    formattingHelper.alignment = Qt.AlignHCenter
                }
            }

            RtfButton {
                height: parent.height
                width: height
                iconSource: "../images/right-align.svg"
                active: formattingHelper.alignment & Qt.AlignRight
                onClicked: {
                    formattingHelper.alignment = Qt.AlignRight
                }
            }
        }

        Row {
            anchors.left: parent.left
            anchors.right: parent.right
            anchors.leftMargin: units.gu(1)
            anchors.rightMargin: units.gu(1)
            height: units.gu(4)

            RtfButton {
                iconName: "select"
                height: parent.height
                width: height
                onClicked: {
                    var pos = noteTextArea.cursorPosition
                    noteTextArea.insert(pos, "<img src=\"image://theme/select-none\" height=" + units.gu(2) + ">")
                    noteTextArea.cursorPosition = pos + 1;
                }
            }

            RtfButton {
                iconName: "attachment"
                height: parent.height
                width: height
                onClicked: {
                    apl.addPageToCurrentColumn(root.page, Qt.resolvedUrl("../ui/ImportPage.qml"),
                                               { note: note, noteTextArea: noteTextArea, insertPosition: noteTextArea.cursorPosition })
                    // priv.insertPosition = noteTextArea.cursorPosition;
                    // note.richTextContent = noteTextArea.text;
                    // importPicker.visible = true;
                    // Qt.inputMethod.hide();
                }
            }

            RtfButton {
                iconName: "navigation-menu"
                height: parent.height
                width: height
                active: toolbox.blockFormatExpanded
                onClicked: {
                    toolbox.blockFormatExpanded = !toolbox.blockFormatExpanded
                }
            }

            RtfButton {
                iconName: "edit-select-all"
                height: parent.height
                width: height
                active: toolbox.charFormatExpanded
                onClicked: {
                    toolbox.charFormatExpanded = !toolbox.charFormatExpanded
                }
            }

            RtfButton {
                iconName: "undo"
                height: parent.height
                width: height
                enabled: formattingHelper.canUndo
                onClicked: {
                    formattingHelper.undo();
                }
            }

            RtfButton {
                iconName: "redo"
                height: parent.height
                width: height
                enabled: formattingHelper.canRedo
                onClicked: {
                    formattingHelper.redo();
                }
            }
        }

        FormattingHelper {
            id: formattingHelper
            textDocument: noteTextArea.textDocument
            cursorPosition: noteTextArea.cursorPosition
            selectionStart: noteTextArea.selectionStart
            selectionEnd: noteTextArea.selectionEnd
        }

        Component {
            id: fontPopoverComponent
            Popover {
                id: fontPopover

                property int selectionStart: -1
                property int selectionEnd: -1

                ListView {
                    width: parent.width - units.gu(2)
                    height: units.gu(30)
                    model: formattingHelper.allFontFamilies
                    clip: true
                    delegate: Empty {
                        height: units.gu(6)
                        width: parent.width
                        Label {
                            anchors.fill: parent
                            anchors.margins: units.gu(1)
                            verticalAlignment: Text.AlignVCenter
                            text: modelData
                            font.family: modelData
                        }
                        onClicked: {
                            noteTextArea.cursorPosition = fontPopover.selectionStart;
                            noteTextArea.moveCursorSelection(fontPopover.selectionEnd);
                            formattingHelper.fontFamily = modelData;
                            PopupUtils.close(fontPopover)
                        }
                    }
                }
            }
        }

        Component {
            id: fontSizePopoverComponent
            Popover {
                id: fontSizePopover

                property int selectionStart: -1
                property int selectionEnd: -1

                ListView {
                    anchors { left: parent.left; right: parent.right; top: parent.top }
                    height: units.gu(30)
                    clip:true
                    model: ListModel {
                        ListElement { modelData: "8" }
                        ListElement { modelData: "10" }
                        ListElement { modelData: "12" }
                        ListElement { modelData: "14" }
                        ListElement { modelData: "18" }
                        ListElement { modelData: "24" }
                        ListElement { modelData: "36" }
                    }

                    delegate: Empty {
                        Label {
                            anchors.fill: parent
                            anchors.margins: units.gu(1)
                            verticalAlignment: Text.AlignVCenter
                            text: modelData
                            font.family: modelData
                        }
                        onClicked: {
                            noteTextArea.cursorPosition = fontSizePopover.selectionStart;
                            noteTextArea.moveCursorSelection(fontSizePopover.selectionEnd);
                            formattingHelper.fontSize = modelData;
                            PopupUtils.close(fontSizePopover)
                        }
                    }
                }
            }
        }

        Component {
            id: colorPopoverComponent
            Popover {
                id: colorPopover

                property int selectionStart: -1
                property int selectionEnd: -1

                GridView {
                    id: colorsGrid
                    anchors { left: parent.left; right: parent.right; top: parent.top; margins: units.gu(0.5) }
                    height: cellWidth * 5 + units.gu(1)

                    cellWidth: width / 8
                    cellHeight: cellWidth

                    model: ListModel {
                        ListElement { color: "#000000" }
                        ListElement { color: "#993300" }
                        ListElement { color: "#333300" }
                        ListElement { color: "#003300" }
                        ListElement { color: "#003366" }
                        ListElement { color: "#000080" }
                        ListElement { color: "#333399" }
                        ListElement { color: "#333333" }

                        ListElement { color: "#800000" }
                        ListElement { color: "#ff6600" }
                        ListElement { color: "#808000" }
                        ListElement { color: "#008000" }
                        ListElement { color: "#008080" }
                        ListElement { color: "#0000ff" }
                        ListElement { color: "#666699" }
                        ListElement { color: "#808080" }

                        ListElement { color: "#ff0000" }
                        ListElement { color: "#ff6600" }
                        ListElement { color: "#99CC00" }
                        ListElement { color: "#339966" }
                        ListElement { color: "#33CCCC" }
                        ListElement { color: "#3366FF" }
                        ListElement { color: "#800080" }
                        ListElement { color: "#999999" }

                        ListElement { color: "#ff00ff" }
                        ListElement { color: "#ffcc00" }
                        ListElement { color: "#ffff00" }
                        ListElement { color: "#00ff00" }
                        ListElement { color: "#00ffff" }
                        ListElement { color: "#00ccff" }
                        ListElement { color: "#993366" }
                        ListElement { color: "#c0c0c0" }

                        ListElement { color: "#ff99cc" }
                        ListElement { color: "#ffcc99" }
                        ListElement { color: "#ffff99" }
                        ListElement { color: "#ccffcc" }
                        ListElement { color: "#ccffff" }
                        ListElement { color: "#99ccff" }
                        ListElement { color: "#cc99ff" }
                        ListElement { color: "#ffffff" }
                    }
                    delegate: AbstractButton {
                        width: colorsGrid.cellWidth
                        height: colorsGrid.cellHeight
                        LomiriShape {
                            anchors.fill: parent
                            anchors.margins: units.gu(.5)
                            color: model.color
                            radius: "small"
                        }
                        onClicked: {
                            noteTextArea.cursorPosition = colorPopover.selectionStart;
                            noteTextArea.moveCursorSelection(colorPopover.selectionEnd);
                            formattingHelper.color = color
                            PopupUtils.close(colorPopover)
                        }
                    }
                }
            }
        }
    }
}
