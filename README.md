# Lomiri Notes App

Lomiri Notes App is a note taking app for Lomiri, powered by the Evernote cloud
API.

## i18n: Translating lomiri-notes-app into your Language

You can easily contribute to the localization of this project (i.e. the
translation into your language) by visiting (and signing up with) the
Hosted Weblate service:
https://hosted.weblate.org/projects/lomiri/lomiri-notes-app

The localization platform of this project is sponsored by Hosted Weblate
via their free hosting plan for Libre and Open Source Projects.

## Building the app

### For the desktop

Install the build dependencies listed in debian/control.

Then run the following commands from inside the project directory:

    mkdir build
    cd build
    cmake -DCLICK_MODE=OFF ..
    make

It can be installed using:

    make install

### For a device

To build a click package for the device run:

    clickable build --all --arch arm64 # or amd64 / armhf

Ensure that the device is in developer mode, connect it to your build machine,
then use `clickable install` in order to install it on the device.

See [clickable documentation](https://clickable-ut.dev/en/latest/) for details.
